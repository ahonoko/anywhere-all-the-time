package com.jg.sound.app;

import processing.core.*;

interface Gui_Button_Listener{
	public void pressed(String id);
}

public class Gui_Button {
	Gui_Button_Listener listener;
	PApplet parent;
	String id; 
	int x, y;
	int size_x, size_y;
	int basecolor;
	int currentcolor;
	int roundness = 1;
	String text;
	int textcolor = 0;
	int[] textpos = new int[2];
	int textsize;
	boolean fingerdown;
	int bottom, right;
	
	Gui_Button(PApplet p, Gui_Button_Listener l, String idstring, int ix, int iy, int isize_x, int isize_y, int icolor, String txt, int textsiz){
		id = idstring;
		listener = l;
		parent = p;
	    x = ix;
	    y = iy;
	    size_x = isize_x;
	    size_y = isize_y;
	    basecolor = icolor;
	    text = txt;
	    
	    textpos[0] = x + (size_x / 2);
	    textpos[1] = y + (size_y / 2); 
	    
	    textsize = textsiz ;
	    
	    right = x + size_x; 
	    bottom = y + size_y;
	    
	}
	

	void update() {
		
	    parent.textSize(textsize);
	   

		
		if ( parent.mousePressed && overRect(x, y, size_x, size_y) ){
			// toggle state, but only if state was not just toggled
			if (fingerdown == false){
				listener.pressed(id);
				fingerdown = true;
			}
		} else {
			fingerdown = false;
		}
		display();
	}
	
	void display(){
//	    parent.stroke(200);
	    parent.noStroke();
	    parent.textSize(textsize);
		parent.fill(basecolor);
		parent.rect(x, y, size_x, size_y, roundness);
		parent.textAlign(PApplet.CENTER, PApplet.CENTER);
		parent.fill(textcolor);
	    parent.text(text, textpos[0], textpos[1]);
	    
	}
	
	
	boolean overRect(int x, int y, int width, int height) {
	    if (parent.mouseX >= x && parent.mouseX <= x+width && 
	    		parent.mouseY >= y && parent.mouseY <= y+height) {
	      return true;
	    } else {
	      return false;
	    }
	  }
	
	
}
