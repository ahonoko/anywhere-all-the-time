/**
 * 
anywhere all the time
 */

//
//any
package com.jg.sound.app;

/*to do Aki

reset wifi values after wifi off

 */

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;

import org.puredata.android.io.AudioParameters;
import org.puredata.android.service.PdPreferences;
import org.puredata.android.service.PdService;
import com.jg.sound.app.R;
import org.puredata.android.utils.PdUiDispatcher;
import org.puredata.core.PdBase;
import org.puredata.core.PdListener;
//import org.puredata.core.PdListener;
import org.puredata.core.PdReceiver;
import org.puredata.core.utils.IoUtils;
import org.puredata.core.utils.PdDispatcher;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.provider.SyncStateContract.Constants;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import processing.core.*;


public class AAT_main extends PApplet implements OnClickListener, OnEditorActionListener, SharedPreferences.OnSharedPreferenceChangeListener {


	private Handler wifi_PD_handler = new Handler();
	

	
	private int this_send_wifi_index = 0;
	
	private float ac_sens_x,ac_sens_y,ac_sens_z;
	
	private static final String TAG = "Pd Test";
	private boolean visible = true;
	
	private String ver_info = "ver 141225 1803";
	
	private int pd_state = 0;
	
	private int pd_send_flag = 1;
	private int pd_send_flag_max = 30;///how often send to PD. once per x time loop.
	
	private static final int MENU_ID_MENU1 = (Menu.FIRST + 1);

	
	Calendar calendar;
	
	WifiManager wifi_main;
	
	private EditText msg;

	private String log_str;
	
	protected PdUiDispatcher dispatcher;
	
	private PdService pdService = null;

	private Toast toast = null;
	
	private Global_data glo_data;
	
	private int max_ap_num = 5;
	
	
	private String[][] wifi_info_ary = new String[max_ap_num][11];
	private float wifi_num = 0;
	private float[] gps_info_ary = new float[3];
	private float[] rot_ary = new float[3];
	private float[] time_ary = new float[6];
	
	private Object[] list_PD_ssid,list_PD_mac,list_PD_wifi_info,list_PD_gps,list_PD_rot,list_PD_date;
	
	
	private Object PD_wifi;
	
	private int test_val;
	
	private float PD_val_str_0,PD_val_str_1;
	
	Gui_Processing_Main GUI;
	
	private void toast(final String msg) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (toast == null) {
					toast = Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT);
				}
				toast.setText(TAG + ": " + msg);
				toast.show();
			}
		});
	}

	private void post(final String s) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {

			}
		});
	}


	
	private PdReceiver receiver = new PdReceiver() {

		private void pdPost(String msg) {
			toast("Pure Data says, \"" + msg + "\"");
		}

		@Override
		public void print(String s) {
			post(s);
		}

		@Override
		public void receiveBang(String source) {
			pdPost("bang");
		}

		@Override
		public void receiveFloat(String source, float x) {
			pdPost("float: " + x);
			log_str = String.valueOf(x);
			Log.d("PD_val_float", String.valueOf(x) );
		}

		@Override
		public void receiveList(String source, Object... args) {
			pdPost("list: " + Arrays.toString(args));
			
			
			Log.d("PD_val_list", Arrays.toString(args) );
		}

		@Override
		public void receiveMessage(String source, String symbol, Object... args) {
			pdPost("message: " + Arrays.toString(args));
			
			Log.d("PD_val_msg", Arrays.toString(args) );
			
		}

		@Override
		public void receiveSymbol(String source, String symbol) {
			pdPost("symbol: " + symbol);
			
			Log.d("PD_val_symbol", symbol );
		}
	};

		
	private final ServiceConnection pdConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			pdService = ((PdService.PdBinder)service).getService();
			initPd();
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			// this method will never be called
		}
	};

	public void setup(){
		
		Log.d("AAT_main:","-----setup" );
		
		GUI.setup(glo_data.get_life_flag());
		
		background(0,255,0);
		
		test_val =0;
		
		log_str = "";
		
		
		if (pdService.isRunning()) {

		} else {
			glo_data.set_life_flag(1);
			
			startAudio();
			
		}
		
	}
	
	
	public void draw(){
		
		GUI.draw();
		
		///main BG
		//fill(255,0,0);
		//rect(0,0,855,555);
		
		//get date
		calendar = Calendar.getInstance();

		
		glo_data.set_time_ary((float)(calendar.get(Calendar.SECOND)), 0);
		glo_data.set_time_ary((float)(calendar.get(Calendar.MINUTE)), 1);
		glo_data.set_time_ary((float)(calendar.get(Calendar.HOUR_OF_DAY)), 2);
		glo_data.set_time_ary((float)(calendar.get(Calendar.DAY_OF_MONTH)), 3);
		glo_data.set_time_ary((float)(calendar.get(Calendar.MONTH)+1), 4);
		glo_data.set_time_ary((float)(calendar.get(Calendar.YEAR)), 5);
		
		if(pd_send_flag == 1){
			copy_glo_data();
			send_to_PD_delay();
		}
		
		pd_send_flag++;
		if(pd_send_flag>pd_send_flag_max){
			pd_send_flag = 1;
		}
		
		
	}
	
	public void copy_glo_data(){
		
		wifi_info_ary = glo_data.get_wifi_info_ary();
		wifi_num = glo_data.get_wifi_num();
		gps_info_ary = glo_data.get_gps_info_ary();
		rot_ary = glo_data.get_rot_ary();
		time_ary = glo_data.get_time_ary();
		
	}
	
	
	public void send_to_PD_delay(){
		
		Log.d("send_to-PD:","-------------------START" );
		
		if(glo_data.get_wifi_grab_flag()==1){
		
		wifi_PD_handler.postDelayed( send_to_PD_wifi_delay, 15);
		Log.d("send_to-PD_wifi_num:","-----wifi_num start" );
		
		try {
			PdBase.sendFloat("wifi_num", wifi_num);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
		Log.d("Final_sendPD_wifi_num:", String.valueOf(wifi_num));
		Log.d("send_to-PD_wifi_num:","-----wifi_num END" );
		
		
		}
		
		if(glo_data.get_gps_grab_flag() == 1){
			send_to_PD_GPS();
			
		}
		
		send_to_PD_Sensor();

	}
	
	private final Runnable send_to_PD_wifi_delay = new Runnable() {
	    @Override
	    public void run() {
	    	
	    		try {
					send_to_PD_wifi(this_send_wifi_index);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
	    		
	    		wifi_PD_handler.postDelayed( send_to_PD_wifi_delay, 15);
	    		this_send_wifi_index++;
	    		
	    		if(this_send_wifi_index == max_ap_num){
	    			this_send_wifi_index = 0;
	    			wifi_PD_handler.removeCallbacks(send_to_PD_wifi_delay);

	    			Log.d("send_to-PD:","-------------------END" );
	    			glo_data.set_wifi_grab_flag(0);
	    			
	    		}
	    		
	    }
	};
	
	public void send_to_PD_wifi(int this_wifi_index){
		
		Log.d("Final_sendPD_wifi:",String.valueOf(this_wifi_index) +"------------------------" );
		

		
		for(int i = 0; i<2; i++){
			float this_float = Float.parseFloat(wifi_info_ary[this_wifi_index][i+2]);
			list_PD_ssid[i] = this_float;
			
			Log.d("Final_sendPD_ssid:",String.valueOf(i) +":"+ String.valueOf(this_float) );
		}
		
		for(int i = 0; i<4; i++){
			float this_float = Float.parseFloat(wifi_info_ary[this_wifi_index][i+4]);
			list_PD_mac[i] = this_float;
			
			Log.d("Final_sendPD_mac:",String.valueOf(i) +":"+ String.valueOf(this_float) );
		}
		
		for(int i = 0; i<3; i++){
			float this_float = Float.parseFloat(wifi_info_ary[this_wifi_index][i+8]);
			list_PD_wifi_info[i] = this_float;
			
			Log.d("Final_sendPD_wifi_info:",String.valueOf(i) +":"+ String.valueOf(this_float) );
		}
		
		PdBase.sendList("ssid" + String.valueOf(this_wifi_index+1), list_PD_ssid);
		PdBase.sendList("mac" + String.valueOf(this_wifi_index+1), list_PD_mac);
		PdBase.sendList("wifi_info" + String.valueOf(this_wifi_index+1), list_PD_wifi_info);
	}
	
	public void send_to_PD_GPS(){
		
		Log.d("send_to-PD_GPS:","-----GPS start" );
		for(int i = 0; i<3; i++ ){
			list_PD_gps[i] = gps_info_ary[i];
			Log.d("Final_sendPD_GPS:",String.valueOf(i) +":"+ String.valueOf(gps_info_ary[i]) );
			
		}
		try {
			PdBase.sendList("gps", list_PD_gps);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		glo_data.set_gps_grab_flag(0);
		Log.d("send_to-PD_GPS:","-----GPS end" );
	}
	
	public void send_to_PD_Sensor(){
		Log.d("send_to-PD_Sensor:","-----SENSOR start" );
		///rot
		for(int i = 0; i<3; i++ ){
			list_PD_rot[i] = rot_ary[i];
		}
		try {
			PdBase.sendList("rotation", list_PD_rot);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		Log.d("send_to-PD_Sensor:","-----SENSOR `END" );
	}
	
	public void send_to_PD_time(){
		Log.d("send_to-PD_TIME:","-----TIME start" );
		///date
		for(int i = 0; i<6; i++ ){
			list_PD_date[i] = time_ary;
		}
		PdBase.sendList("date", list_PD_date);
		Log.d("send_to-PD_TIME:","-----TIME  END" );
	}
	
	@Override
	public void onCreate(android.os.Bundle savedInstanceState) {
		
	Log.d("AAT_main:","-----oncreate" );
		super.onCreate(savedInstanceState);

		Log.d("AAT_main:","-----oncreated-----" );
		
		//init global data
		glo_data = (Global_data)getApplication();
		glo_data.init();
		
		
		 GUI = new Gui_Processing_Main(this, glo_data.get_gps_info_ary());
		
		// Keep screen on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
        ///// message dialog for chk GPS at startup
        AlertDialog.Builder alertDlg = new AlertDialog.Builder(this);
        alertDlg.setTitle("Turn on GPS");
        alertDlg.setMessage("Turn on GPS");
        alertDlg.setPositiveButton(
            "GPS setting",
            new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // after OK button pressed
                		
            		Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            		startActivity(intent);
                	
                }
            });
        
        ///wifi
        wifi_main = (WifiManager)getSystemService(WIFI_SERVICE);
        
		AudioParameters.init(this);
		PdPreferences.initPreferences(getApplicationContext());
		PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).registerOnSharedPreferenceChangeListener(this);
		//initGui();
		bindService(new Intent(this, PdService.class), pdConnection, BIND_AUTO_CREATE);
		
		
		///init local arrays
		
		for(int i = 0; i<max_ap_num; i++){
	    	for(int j = 0; j<11; j++){
	    		wifi_info_ary[i][j] = "0";
	    		}
	    }
	    
	    ///gps
	    for(int i = 0; i<3; i++){
	    	gps_info_ary[i] = 0;
	    }
	    
	    ///sensor
	    for(int i = 0; i<3; i++){

	    	rot_ary[i] = 0;

	    }
	    
	    ///time, sec, min, hour, day, month, year 
	    for(int i = 0; i<6; i++){
	    	time_ary[i] = 0;
	    }
	    
		
		///init local PD list objects for info
		
		list_PD_ssid = new Object[2];
		list_PD_mac = new Object[4];
		list_PD_wifi_info = new Object[3];
		
		list_PD_gps = new Object[3];
		
		PD_wifi = new Object[11];
		
		list_PD_rot = new Object[3];
		
		list_PD_date = new Object[6];
		
		///start service info grabber
		
		//chk service
		if(!isServiceRunning("Device_info_grabber.class")){
			Log.d("main_onCreate", "staring Device_info_grabber..");
			startService(new Intent(getBaseContext(),Device_info_grabber.class));
		}
		
		
		
		
		////chk wifi.
   		if(wifi_main.isWifiEnabled()){
   			
   		}else{
			wifi_main.setWifiEnabled(true);
		}
   		
   		/// chk GPS
		
   		String providers = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
   		if(providers.indexOf("gps", 0) < 0) {
   			///dialog for GPS
   				alertDlg.create().show();
   			}
   		
   		
		
	};

	
	@Override
	protected void onResume() {
		super.onResume();
		
	}
	
	@Override
	protected void onPause() {	
		
		Log.d("AAT_main:","-----onPause" );
		super.onPause();

	}
	
	@Override
	public void onDestroy() {
		
		Log.d("AAT_main:","-----onDestroy" );
		
		stopService(new Intent(getBaseContext(),Device_info_grabber.class));
		super.onDestroy();
		cleanup();
		
	}
	
	@Override
	public void onStop(){
		Log.d("AAT_main:","-----onStop" );
		super.onStop();
		
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		if (pdService.isRunning()) {
			startAudio();
		}
	}

	
	private void initPd() {
		
		File dir = getFilesDir();
		File patchFile = new File(dir, "ssid_4.pd");
		try{
			IoUtils.extractZipResource(getResources().openRawResource(R.raw.patch), dir, true);
			PdBase.openPatch(patchFile.getAbsolutePath());
			
		}catch (IOException e) {
			Log.e(TAG, e.toString());
			finish();
		}
		
		
		/*
		listen to PD
		*/
		
		dispatcher = new PdUiDispatcher();
		PdBase.setReceiver(dispatcher);
		
		dispatcher.addListener("vu", GUI);
		
	}

	private void startAudio() {
		String name = getResources().getString(R.string.app_name);
		try {
			pdService.initAudio(-1, -1, -1, -1);   // negative values will be replaced with defaults/preferences
			pdService.startAudio(new Intent(this, AAT_main.class), R.drawable.icon, name, "Return to " + name + ".");
			
			
			pd_state = 1;
			
			//toast("pd start");
			
		} catch (IOException e) {
			toast(e.toString());
		}
	}

	private void stopAudio() {
		pdService.stopAudio();
		
	}
	
	private void cleanup() {
		try {
			unbindService(pdConnection);
		} catch (IllegalArgumentException e) {
			// already unbound
			pdService = null;
		}
	}
	
	boolean isServiceRunning(String className) {
	    ActivityManager am = (ActivityManager)getSystemService(ACTIVITY_SERVICE);
	    List<ActivityManager.RunningServiceInfo> serviceInfos = am.getRunningServices(Integer.MAX_VALUE);
	    int serviceNum = serviceInfos.size();
	    for (int i = 0; i < serviceNum; i++) {
	        if (serviceInfos.get(i).service.getClassName().equals(className)) {
	            return true;
	        }
	    }
	    return false;
	}
	
	//menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        menu.add(Menu.NONE, MENU_ID_MENU1, Menu.NONE, "Exit from Anywhere All The Time");
        
        return super.onCreateOptionsMenu(menu);
    }
 
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

    	return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean ret = true;
        switch (item.getItemId()) {
        default:
            ret = super.onOptionsItemSelected(item);
            break;
        case MENU_ID_MENU1:
        	
        	this.finish();

            break;
 
        }
        
        return ret;
    }

	@Override
	public void onClick(View v) {
		
	}

		
	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		evaluateMessage(msg.getText().toString());
		return true;
	}

	private void evaluateMessage(String s) {
		String dest = "test", symbol = null;
		boolean isAny = s.length() > 0 && s.charAt(0) == ';';
		Scanner sc = new Scanner(isAny ? s.substring(1) : s);
		if (isAny) {
			if (sc.hasNext()) dest = sc.next();
			else {
				toast("Message not sent (empty recipient)");
				return;
			}
			if (sc.hasNext()) symbol = sc.next();
			else {
				toast("Message not sent (empty symbol)");
			}
		}
		List<Object> list = new ArrayList<Object>();
		while (sc.hasNext()) {
			if (sc.hasNextInt()) {
				list.add(Float.valueOf(sc.nextInt()));
			} else if (sc.hasNextFloat()) {
				list.add(sc.nextFloat());
			} else {
				list.add(sc.next());
			}
		}
		if (isAny) {
			PdBase.sendMessage(dest, symbol, list.toArray());
		} else {
			switch (list.size()) {
			case 0:
				PdBase.sendBang(dest);
				break;
			case 1:
				Object x = list.get(0);
				if (x instanceof String) {
					PdBase.sendSymbol(dest, (String) x);
				} else {
					PdBase.sendFloat(dest, (Float) x);
				}
				break;
			default:
				PdBase.sendList(dest, list.toArray());
				break;
			}
		}
	}
}


	