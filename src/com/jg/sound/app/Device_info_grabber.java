package com.jg.sound.app;

/*
 this is the service that grabs information from wifi, GPS, etc.
 */

//import android.app.ListActivity;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.Notification;
import android.app.NotificationManager;
import android.support.v4.app.NotificationCompat;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;


public class Device_info_grabber extends Service implements LocationListener,
		SensorEventListener {

	final static String TAG = "Device_info_grabber";
	

	private Global_data glo_data;

	private List<ActivityManager.RunningTaskInfo> taskList;

	private List<ScanResult> cfgList;
	private List<ScanResult> wifi_non_sec_list;
	private List<ScanResult> wifi_with_sec_list;
	private List<ScanResult> wifi_with_sec_list_pre;
	private List<ScanResult> wifi_sorted_list;

	float wifi_num;
	private int max_ap_num = 5;

	// / gps 30 sec
	// / wifi 5 sec
	// / acceelo 100 ms

	private SensorManager _sensorManager = null;
	private Sensor mag_sensor;

	private float[] mag = new float[3];

	final int INTERVAL_PERIOD_actchk = 100;
	final int INTERVAL_PERIOD_wifi = 5000;
	final int INTERVAL_PERIOD_gps = 10000;

	Timer timer_actchk = new Timer();
	Timer timer_wifi = new Timer();
	Timer timer_gps = new Timer();

	double gps_lat, gps_lon;
	float gps_accu, gps_spd, gps_bearing;

	LocationManager mLocationManager;

	/*
	 * wifi
	 */

	public void get_wifi_info() {

		glo_data.ser_msg = "start_Wifi";

		WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
		if (wifiManager.startScan()) {

			cfgList = wifiManager.getScanResults();
		}


		if (cfgList != null) {

			glo_data.set_wifi_num(cfgList.size());

			String[] nets = new String[cfgList.size()];

			for (int i = 0; i < cfgList.size(); i++) {

				Log.i("Sample",
						"scan::" + "SSID:" + cfgList.get(i).SSID
								+ ":security caoability:"
								+ cfgList.get(i).capabilities + ":BSSID:"
								+ cfgList.get(i).BSSID + ":freq:"
								+ cfgList.get(i).frequency + ":signal_lv:"
								+ cfgList.get(i).level);
			}

			wifi_non_sec_list.clear();
			wifi_with_sec_list_pre.clear();
			wifi_with_sec_list.clear();
			wifi_sorted_list.clear();

			for (int i = 0; i < cfgList.size(); i++) {
				int wifi_order_index = 0;

				if (chk_sec(cfgList.get(i).capabilities)) {
					wifi_non_sec_list.add(cfgList.get(i));
				} else {
					wifi_with_sec_list.add(cfgList.get(i));
				}
			}
			TreeMap<Integer, Integer> lv_list = new TreeMap<Integer, Integer>();

			for (int i = 0; i < wifi_with_sec_list.size(); i++) {
				lv_list.put(wifi_with_sec_list.get(i).level, i);
			}

			Integer[] indexes = lv_list.values().toArray(new Integer[0]);
			Integer[] sortedValues = lv_list.keySet().toArray(new Integer[0]);

			wifi_sorted_list.addAll(wifi_non_sec_list);

			Log.d("sec_chk: sec_list num",
					String.valueOf(wifi_with_sec_list.size()));
			Log.d("sec_chk: indexes size ", String.valueOf(indexes.length));
			Log.d("sec_chk: sortedValues size ",
					String.valueOf(sortedValues.length));

			for (int i = 0; i < indexes.length - 1; i++) {
				wifi_sorted_list.add(wifi_with_sec_list
						.get(indexes[indexes.length - i - 1]));
			}


			int APnum;

			if (wifi_sorted_list.size() < max_ap_num) {
				APnum = wifi_sorted_list.size();
			} else {

				APnum = max_ap_num;
			}

			Log.i("APNUM:", String.valueOf(APnum));

			// /make APs info
			for (int i = 0; i < APnum; i++) {

				// /SSID
				int SSID_char_num = wifi_sorted_list.get(i).SSID.length();

				Log.i("SSID_char_num:", String.valueOf(SSID_char_num));

				// if(SSID_char_num< 4){
				// /make it 4
				if (SSID_char_num < 4) {

					for (int j = 0; j < SSID_char_num; j++) {
						
						glo_data.set_wifi_info_ary(str_asciier(wifi_sorted_list.get(i).SSID.charAt(j)), i, j);
						
					}
					

					for (int j = SSID_char_num; j < 4; j++) {
						glo_data.set_wifi_info_ary(String.valueOf(0), i, j);

						}

				} else {

					for (int j = 0; j < 4; j++) {

						glo_data.set_wifi_info_ary(str_asciier(wifi_sorted_list.get(i).SSID.charAt(j)), i, j);
						
						
					}
				}

				// /BSSID
				String stripped_BSSID = wifi_sorted_list.get(i).BSSID
						.replaceAll(":", "");
				for (int j = 0; j < 4; j++) {
					glo_data.set_wifi_info_ary(str_asciier(stripped_BSSID.charAt(j)), i, j+4);
					

				}

				// /freqeuncy
				glo_data.set_wifi_info_ary(String.valueOf(wifi_sorted_list.get(i).frequency), i, 8);
				
				glo_data.set_wifi_info_ary(String.valueOf(wifi_sorted_list.get(i).level), i, 9);
				
			}

			///add wifi sec flag
			int this_wifi_sec_index = 0;
			int max_send_wifi = max_ap_num;

			if (wifi_non_sec_list.size() > 0) {

				if (wifi_non_sec_list.size() > max_ap_num) {
					for (int k = 0; k < max_ap_num; k++) {
						Log.i("wifi_non_sec_list:",String.valueOf(wifi_non_sec_list.size()));
						glo_data.set_wifi_info_ary(String.valueOf(0), k, 10);
						this_wifi_sec_index++;
					}

				} else {
					for (int k = 0; k < wifi_non_sec_list.size(); k++) {
						Log.i("wifi_non_sec_list:",String.valueOf(wifi_non_sec_list.size()));
						glo_data.set_wifi_info_ary(String.valueOf(0), k, 10);
						this_wifi_sec_index++;
					}
					int max_wifi_with_sec_num = max_ap_num - wifi_non_sec_list.size();

					for (int k = this_wifi_sec_index; k < max_wifi_with_sec_num
							+ this_wifi_sec_index; k++) {
						glo_data.set_wifi_info_ary(String.valueOf(1), k, 10);
					}

				}

			} else {
				int max_wif_non_sec_num = max_ap_num;

				for (int k = 0; k < max_wif_non_sec_num; k++) {

					glo_data.set_wifi_info_ary(String.valueOf(1), k, 10);
				}

			}

		}

		glo_data.ser_msg = "         end_Wifi";
		glo_data.set_wifi_grab_flag(1);
	}

	/*
	 * END wifi
	 */

	// /str to ascii int
	String str_asciier(char chara) {

		int c_int = (int) chara;

		return String.valueOf(c_int);
	}


	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		// get();

		glo_data = (Global_data) getApplication();



		mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
				0, 0, this);

		Log.d(TAG, "onCreate");
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		Log.d(TAG, "onStartCommand");

		wifi_non_sec_list = new ArrayList();
		wifi_with_sec_list_pre = new ArrayList();
		wifi_with_sec_list = new ArrayList();
		wifi_sorted_list = new ArrayList();


		Notification notification = new NotificationCompat.Builder(
				getApplicationContext()).setContentTitle("content title")
				.setContentText("content text")
				.build();

		NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		manager.notify(1, notification);

		startForeground(1, notification);

		/* Sensor */
		_sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

		mag_sensor = _sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);


		_sensorManager.registerListener(this, mag_sensor,
				SensorManager.SENSOR_DELAY_NORMAL);

		/* Sensor END */

	
		timer_gps.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {

				// /gps
				Log.d(TAG, "get info___GPS");
				get_gps_info();
				
				Log.d(TAG, "get info______END");
			}
		}, 0, INTERVAL_PERIOD_gps);

		timer_wifi.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				// /wifi
				Log.d(TAG, "get info___wifi");
				get_wifi_info();
			}
		}, 0, INTERVAL_PERIOD_wifi);

		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		// / kill timers.
		if (timer_wifi != null) {
			timer_wifi.cancel();
		}
		if (timer_gps != null) {
			timer_gps.cancel();
		}

		// /stop GPS
		mLocationManager.removeUpdates(this);

		// /stop sensor
		if (_sensorManager == null) {
			return;
		}
		_sensorManager.unregisterListener(this);

		Log.d(TAG, "service onDestroy");
	}

	public boolean chk_running_activity(String activity_name) {

		ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		taskList = activityManager.getRunningTasks(5);

		if (taskList != null) {
			for (RunningTaskInfo task : taskList) {
				if (task.topActivity.getClassName().equals(activity_name)) {
					Log.d("chk_running_activity", "app is runing");
					
					return true;
				}

			}
			return false;
		}
		return false;

	}

	/*
	 * GPS
	 */

	public void get_gps_info() {

		Log.i("GPS_info", "lon:" + gps_lon);
		Log.i("GPS_info", "lat:" + gps_lat);
		Log.i("GPS_info", "accu:" + gps_accu);

		glo_data.set_gps_info_ary((float) gps_lon, 0);
		glo_data.set_gps_info_ary((float) gps_lat, 1);
		glo_data.set_gps_info_ary(gps_accu, 2);
		
		glo_data.set_gps_grab_flag(1);
		
	}

	@Override
	public void onLocationChanged(Location location) {

		gps_lon = location.getLatitude() * 100;
		gps_lat = location.getLongitude() * 100;

		gps_accu = location.getAccuracy();

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	/*
	 * GPS END
	 */

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub

		switch (event.sensor.getType()) {

		case Sensor.TYPE_ORIENTATION:

			mag = event.values.clone();

			glo_data.set_rot_ary(mag[0], 0);
			glo_data.set_rot_ary(mag[1], 1);
			glo_data.set_rot_ary(mag[2], 2);
			
			
			break;
		}

	}

	private boolean chk_sec(String sec_cap) {
		boolean sec_flag = false;

		String[] str_spl = sec_cap.split("\\[");

		if (str_spl.length == 2) {
			sec_flag = true;
		} else {
			sec_flag = false;
		}

		return sec_flag;
	}

}
