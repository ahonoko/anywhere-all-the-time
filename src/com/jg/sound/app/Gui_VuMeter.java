package com.jg.sound.app;
import android.util.Log;
import processing.core.*;

public class Gui_VuMeter {

	  PApplet parent;
		
	  int swidth, sheight;    // width and height of bar
	  float xpos, ypos;       // x and y position of bar
	  float spos, newspos;    // x position of slider
	  float sposMin, sposMax; // max and min values of slider
	  int loose;              // how loose/heavy
	  boolean over;           // is the mouse over the slider?
	  boolean locked;
	  float ratio;
	  float last_value;
	  int bottom, right;
	  int steps; // how many steps or "leds" the vu-meter has
	  float fieldwidth;
	  float fieldborder;
	  float targetVal, momentVal;

	  Gui_VuMeter (PApplet p, float xp, float yp, int sw, int sh) {
		parent = p;	
	    swidth = sw;
	    sheight = sh;
	    int widthtoheight = sw - sh;
	    ratio = (float)sw / (float)widthtoheight;
	    xpos = xp;
	    ypos = yp;
	    sposMin = xpos;
	    sposMax = xpos + swidth - sheight;
	    setValue(0); //sets spos
	    newspos = spos;
	    
	    bottom = (int) ypos + sheight;
	    right = (int) xpos + swidth;
	    
	    steps = 10;
	    fieldborder = 2;
	    fieldwidth = (swidth/steps) - fieldborder;
	    

	  }

	  public void update() {	 
		
		// interpolate between values for smoother display
		momentVal = (targetVal - momentVal) * (float)0.2 + momentVal;  
		
	    display(); //draw it
	  }

	  void display() {
	    parent.noStroke();
	    parent.fill(0*255,(int)(0.45 * 255),(int) (0.18 * 255));
	    
	    
	    int fields = PApplet.round( momentVal * steps );
	    for (int i = 0; i < fields; i++){
	    	if (i >= steps * 0.7){
	    		parent.fill(170, 160, 41);
	    	}
	    	parent.rect(xpos + ( fieldwidth * i) + (fieldborder * i), ypos, fieldwidth, sheight);
	    	
	    }
	    
	  }
	  
	  public void setValue(float val) {
		  val = PApplet.constrain(val, 0, 1);
		  targetVal = val;
	  }	
	
}
