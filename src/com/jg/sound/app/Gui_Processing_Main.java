package com.jg.sound.app;
import org.puredata.core.PdBase;
import org.puredata.core.PdListener;

import processing.core.PApplet;
import android.content.Intent;
//import processing.core.PApplet;

interface Gui_Listener{
	
} 

public class Gui_Processing_Main implements Gui_Slider_Listener, Gui_Toggle_Listener, Gui_Button_Listener, PdListener{

	Global_data glo_data;
	PApplet parent;
	Gui_Listener listener; 
	Gui_Toggle[] tog;
	Gui_Slider[] sl;
	Gui_Label[] label;
	Gui_Button[] button;
	Gui_VuMeter[] vu;
	Gui_AnimatedCircle gpsCircle;
	
	int bgcolor;
	int left_border, right_border, top_border;
	int tog_width, tog_height;
	int btn_height, btn_width;
	int sl_height, sl_width;
	int v_dist, h_dist, v_pad; // vertical and horozontal distance between GUI elements
	int h_center; // horizontal middle of the screen
	private boolean initial_values_sent = false;
	  
Gui_Processing_Main(PApplet p, float[] gps_ary){
	
	parent = p;
	
	int toggleCount = 2;
	int sliderCount = 4;
	int labelCount = 5;
	int buttonCount = 2;
	int vuMeterCount = 4;
	
	// GRID / COMMON GUI SETTINGS
	left_border = 20; 
	right_border = 20;
	top_border = 20;
	tog_height = 40; 
	tog_width = 75;
	sl_height = 35;
	sl_width = 170; 
	h_dist = 20; // horizontal distance between GUI elements
	v_pad = 30; // additional vertical distance between
	btn_height = tog_height;
	btn_width = 100;
	
	sl = new Gui_Slider[sliderCount];
	tog = new Gui_Toggle[toggleCount];
	label = new Gui_Label[labelCount];
	button = new Gui_Button[buttonCount];
	vu = new Gui_VuMeter[vuMeterCount];
	
	bgcolor = parent.color(0, 0, 0);
	

}
	

public void setup(int state){
	
//	Log.d("GUI","Application State on Start: " + state);
//	Log.d("GUI","PFont list:");
//	String fontlist[] = PFont.list();
//	for (int i = 0; i < fontlist.length; i++){
//		Log.d("GUI", fontlist[i]);
//	}
	
	
	glo_data = (Global_data)parent.getApplication();
	//glo_data.init();
	
	// calculate vertical distance between GUI objects (800px --> 1 px, 1280 --> 30px)
	v_dist = (int) (0.23 * parent.displayHeight - 99) + 7;
	
	parent.background(bgcolor);
	parent.smooth();
	h_center = parent.width / 2 ;
	
	int togglecolor = parent.color(140);
	int highlight = parent.color(0*255,(int)(0.45 * 255),(int) (0.18 * 255));
	
	
	////////////////////////////////
	// WIFI
	
	// open & protected networks
	tog[0] = new Gui_Toggle(	parent,
								this,
								"toggle_wifi",
								left_border, top_border, 
								tog_width, tog_height, 
								togglecolor, highlight, 
								"WI-FI", "WI-FI");
	tog[0].set(true);
	sl[0] = new Gui_Slider(		parent,
								this,
								"slider_wifi_ssid",
								tog[0].right + h_dist, tog[0].bottom + v_dist, 
								parent.width - (tog[0].right + h_dist + left_border), sl_height, 
								1);
	sl[0].setValue((float)0.8);
	vu[0] = new Gui_VuMeter(	parent, 
								tog[0].right + h_dist + 3, tog[0].bottom + v_dist + 6, 
								parent.width - (tog[0].right + h_dist + left_border) - 1, sl_height - 12);
//	vu[0].setValue(1);
	label[0] = new Gui_Label(	parent, 
								"PROTECTED\n& OPEN WI-FI\nNETWORKS", 
								left_border, (int)sl[0].ypos + sl[0].sheight/2, 
								10);
	
	
	// unique networks
	sl[1] = new Gui_Slider(		parent,
								this,
								"slider_wifi_mac",
								sl[0].xpos, sl[0].bottom + v_dist, 
								parent.width - (tog[0].right + h_dist + left_border), sl_height,
								1);
	
	sl[1].setValue(parent.random((float)0.3, (float)0.6));
	vu[1] = new Gui_VuMeter(	parent, 
								sl[0].xpos + 3, sl[0].bottom + v_dist + 6, 
								parent.width - (tog[0].right + h_dist + left_border) - 1, sl_height - 12);
//	vu[1].setValue(1);
	label[1] = new Gui_Label(	parent, 
								"UNIQUE  WI-FI\nNETWORK  ID'S", 
								left_border, (int)sl[1].ypos + sl[1].sheight/2, 
								10);
	

	
	// network frequency
	sl[2] = new Gui_Slider(		parent,
								this,
								"slider_wifi_network",
								sl[1].xpos, sl[1].bottom + v_dist, 
								parent.width - (tog[0].right + h_dist + left_border), sl_height, 
								1);
	sl[2].setValue(parent.random((float)0.3, (float)0.6));
	vu[2] = new Gui_VuMeter(	parent, 
								sl[2].xpos + 3, sl[1].bottom + v_dist + 6, 
								parent.width - (tog[0].right + h_dist + left_border) - 1, sl_height - 12);
//	vu[2].setValue(1);
	label[2] = new Gui_Label(	parent, 
								"NETWORK\nFREQUENCY", 
								left_border, (int)sl[2].ypos + sl[2].sheight/2, 
								10);

	////////////////////////////////
	// GPS
	tog[1] = new Gui_Toggle(	parent,
								this,
								"toggle_gps",
								left_border, sl[2].bottom + v_dist + v_pad, 
								tog_width, tog_height, 
								togglecolor, highlight, 
								"GPS", "GPS");
	sl[3] = new Gui_Slider(		parent,
								this,
								"slider_gps",
								tog[1].right + h_dist, tog[1].bottom + v_dist, 
								parent.width - (tog[1].right + h_dist + left_border), sl_height, 
								1);
	sl[3].setValue((float)0.7);
	vu[3] = new Gui_VuMeter(	parent, 
								sl[3].xpos + 3, tog[1].bottom + v_dist + 6, 
								parent.width - (tog[1].right + h_dist + left_border) - 1, sl_height - 12);
//	vu[3].setValue(1);
	label[3] = new Gui_Label(	parent,
								"searching\nfor GPS...", 
								left_border, (int)sl[3].ypos + sl[3].sheight/2, 
								10);

	// Animated Circle for GPS State
	gpsCircle = new Gui_AnimatedCircle(parent, (int)sl[3].xpos - 25 , (int)sl[3].ypos + sl[3].sheight/2, 25);	
	
	// BUTTONS
	button[0] = new Gui_Button(	parent, 
								this, 
								"btn_deactivateWifi",	
								(int)sl[0].xpos + 30, tog[0].y, 
								btn_width, btn_height,  
								parent.color(170, 160, 41), 
								"DEACTIVATE WI-FI\nON YOUR DEVICE",
								10);
	
	button[1] = new Gui_Button(	parent, 
								this, 
								"btn_deactivateGPS",	
								(int)sl[0].xpos + 30, tog[1].y, 
								btn_width, btn_height, 
								parent.color(170, 160, 41), 
								"DEACTIVATE GPS\nON YOUR DEVICE",
								10);

	
	
}


public void draw(){
	
	parent.background(bgcolor);
	
	parent.fill(parent.color(255));
	parent.textSize(10);

	// after one second send initial values to pd
	if (parent.frameCount >= parent.frameRate && initial_values_sent == false){
		tog[0].set(true);
		sl[0].setValue(sl[0].getValue());
		sl[1].setValue(sl[1].getValue());
		sl[2].setValue(sl[2].getValue());
		sl[3].setValue(sl[3].getValue());
		initial_values_sent  = true;
	}
	
	
	if(glo_data.get_gps_info_ary()[0] == 0 && glo_data.get_gps_info_ary()[0] == 0 && glo_data.get_gps_info_ary()[0] == 0){
		gpsCircle.animate(true);
	} else {
		gpsCircle.animate(false);
		gpsCircle.setColor(parent.color(100));
		label[3].setText("receiving\nGPS data");
	}
	gpsCircle.update();
	
	
	// update and draw gui elements
	for (int i = 0; i < tog.length; i++){
		if (tog[i] != null) tog[i].update();
	}
	for (int i = 0; i < vu.length; i++){
		if (vu[i] != null) vu[i].update();
	}
	for (int i = 0; i < sl.length; i++){
		if (sl[i] != null) sl[i].update();
	}	
	for (int i = 0; i < label.length; i++){
		if (label[i] != null) label[i].update();
	}
	for (int i = 0; i < button.length; i++){
		if (button[i] != null) button[i].update();
	}



	
}




// TOGGLE CALLBACKS
@Override
public void valueChanged(String id, boolean state) {
	
	if (state) PdBase.sendFloat(id, 1);
	else PdBase.sendFloat(id, 0);
	
}


// SLIDER CALLBACKS
@Override
public void valueChanged(String id, float newval) {
	PdBase.sendFloat(id, newval);
}


@Override
public void receiveBang(String source) {
	// TODO Auto-generated method stub
	
}


@Override
public void receiveFloat(String source, float x) {
//	Log.i("GUI", String.valueOf(x) );
	
}


@Override
public void receiveSymbol(String source, String symbol) {
//	Log.i("GUI", symbol );
	
}


@Override
public void receiveList(String source, Object... args) {
//	Log.i("GUI", String.valueOf(args[0]) + " " + String.valueOf(args[1])	 );
	for (int i = 0; i < args.length && i < vu.length; i++)
		vu[i].setValue((Float)args[i]); 
}


@Override
public void receiveMessage(String source, String symbol, Object... args) {
	// TODO Auto-generated method stub
	
}

// Gui_Button Callbacks
@Override
public void pressed(final String id) {
	
	if(id == "btn_deactivateWifi"){
		
		parent.runOnUiThread(new Runnable() {
    		final Intent intent = new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS);
    		
    		public void run(){
    			parent.startActivity(intent);
    		}
		});
	} else if (id == "btn_deactivateGPS"){
		parent.runOnUiThread(new Runnable() {
    		final Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
    		
    		public void run(){
    			parent.startActivity(intent);
    		}
		});
	}
	
}



	
}