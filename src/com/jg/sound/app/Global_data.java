package com.jg.sound.app;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Intent;
import android.provider.SyncStateContract.Constants;
import android.util.Log;

public class Global_data extends Application{
	
	
	int main_app_life_flag = 0;
	
	int wifi_grab_flag = 0;
	int gps_grab_flag = 0;
	
	private static int max_ap_num = 5;
	
	 private static String[][] wifi_info_ary = new String[max_ap_num][11];
	 private static float wifi_num = 0;
	  
	 private static float[] gps_info_ary = new float[3];
	  
	 private static float[] rot_ary = new float[3];
	 private static float[] time_ary = new float[6];
	  
	  String ser_msg;
	
	  @Override
	  public void onCreate(){
		  super.onCreate();
		  
	  }
	  
	  
    public void init(){
    	
    	///setup arrays
    	///wifi info
	    for(int i = 0; i<max_ap_num; i++){
	    	for(int j = 0; j<11; j++){
	    		wifi_info_ary[i][j] = "0";
	    		}
	    }
	    
	    ///gps
	    for(int i = 0; i<3; i++){
	    	gps_info_ary[i] = 0;
	    }
	    
	    ///sensor
	    for(int i = 0; i<3; i++){
	    	rot_ary[i] = 0;
	    }
	    
	    ///time, sec, min, hour, day, month, year 
	    for(int i = 0; i<6; i++){
	    	time_ary[i] = 0;
	    }
	    
	    ser_msg ="";
    }
    /*
    getter
    */
    public synchronized String[][] get_wifi_info_ary(){
    		return wifi_info_ary;
    }
    
    public synchronized float get_wifi_num(){
    		return wifi_num;
    }
    
    public synchronized float[] get_gps_info_ary(){
    		return gps_info_ary;
    }
    
    public synchronized float[] get_rot_ary(){
		return rot_ary;
    }
    
    public synchronized float[] get_time_ary(){
		return time_ary;
    }
    
    public synchronized int get_life_flag(){
    		return main_app_life_flag;
    }
    
    public synchronized int get_wifi_grab_flag(){
		return wifi_grab_flag;
    }
    
    public synchronized int get_gps_grab_flag(){
		return gps_grab_flag;
    }
    
    /*
     * setter
     */
    
    public synchronized void set_wifi_info_ary(String s,int g, int index){
    		wifi_info_ary[g][index] = s;
    }
    
    public synchronized void set_wifi_num(float f){
		wifi_num = f;
}
    
    public synchronized void set_gps_info_ary(float f, int index){
    		gps_info_ary[index] = f;
    }
    
    public synchronized void set_rot_ary(float f, int index){
		rot_ary[index] = f;
    }
    
    public synchronized void set_time_ary(float f, int index){
    		time_ary[index] = f;
    }
    
    public synchronized void set_life_flag(int i){
    		main_app_life_flag = i;
    }
    
    public synchronized void set_wifi_grab_flag(int i){
    		wifi_grab_flag = i;
    }
    
    public synchronized void set_gps_grab_flag(int i){
		gps_grab_flag = i;
    }
    
}
