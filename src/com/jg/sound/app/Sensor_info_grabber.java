package com.jg.sound.app;


import java.util.List;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class Sensor_info_grabber implements SensorEventListener {

	
	private SensorManager _sensorManager = null;
	private float _x, _y, _z;

	private Global_data glo_data;
	
	public void onCreate(Context c) {
		
		//glo_data = (Global_data)getApplication();
		
		_sensorManager = (SensorManager) c.getSystemService(Context.SENSOR_SERVICE);
		onResume();
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		//今回は使用しません。
	}

	//アクティビティが動き始めたらリスナーを登録する
	public void onResume() {
		List<Sensor> sensorList = _sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER);
		if (sensorList != null && !sensorList.isEmpty()) {
			Sensor sensor = sensorList.get(0);
			_sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_FASTEST);
		}
	}

	//アクティビティがポーズになったらリスナーを止める
	public void onPause() {
		if( _sensorManager == null ){
			return;
		}
		_sensorManager.unregisterListener(this);
	}

	//センサーの値に変化があった時呼ばれる
	public void onSensorChanged(SensorEvent event) {
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			_x = event.values[SensorManager.DATA_X]; // X軸
			_y = event.values[SensorManager.DATA_Y]; // Y軸
			_z = event.values[SensorManager.DATA_Z]; // Z軸
			
			/*
			glo_data.accerelo_ary[0] = _x;
			glo_data.accerelo_ary[1] = _y;
			glo_data.accerelo_ary[2] = _z;
			*/
		}
	}
	
	public float getX(){
		return _x;
	}
	
	public float getY(){
		return _y;
	}
	
	public float getZ(){
		return _z;
	}

	//シングルトン
	private static Sensor_info_grabber _instance = new Sensor_info_grabber();
	private Sensor_info_grabber() {
		_x = _y = _z = 0;
	}
	public static Sensor_info_grabber Inst() {
		return _instance;
	}
}
