package com.jg.sound.app;

import processing.core.*;

interface Gui_Toggle_Listener{
	public void valueChanged(String id, boolean state);
}

public class Gui_Toggle {
	Gui_Toggle_Listener listener;
	PApplet parent;
	String id; 
	int x, y;
	int size_x, size_y;
	int bottom, right;
	int basecolor, highlightcolor;
	int currentcolor;
	int roundness = 1;
	boolean state = false; // true is on, false is off
	boolean fingerdown = false;
	String ontext, offtext;
	int textcolor = 0;
	int[] textpos = new int[2];
	int textsize;
	
	Gui_Toggle(PApplet p, Gui_Toggle_Listener l, String idstring, int ix, int iy, int isize_x, int isize_y, int icolor, int ihighlight, String ontxt, String offtxt){
		id = idstring;
		listener = l;
		parent = p;
	    x = ix;
	    y = iy;
	    size_x = isize_x;
	    size_y = isize_y;
	    basecolor = icolor;
	    highlightcolor = ihighlight;
	    ontext = ontxt;
	    offtext = offtxt; 
	    textsize = size_y - 15;
	    
	    right = x + size_x;
	    bottom = y + size_y;
//	    currentcolor = basecolor;
	    textpos[0] = x + (size_x / 2);
	    textpos[1] = y + (size_y / 2);

	}
	

	void update() { 
		
		if ( parent.mousePressed && overRect(x, y, size_x, size_y) ){
			// toggle state, but only if state was not just toggled
			if (fingerdown == false){
				
				state = !state;
				listener.valueChanged(id, state);
				fingerdown = true;
			}
		} else {
			fingerdown = false;
		}
		display();
	}
	
	void display(){
//	    parent.stroke(255);
	    parent.noStroke();
	    parent.textSize(textsize);
	    if (state == true) {
	    	parent.fill(highlightcolor);
	    	parent.rect(x, y, size_x, size_y, roundness);
	    	parent.fill(textcolor);
	    	parent.textAlign(PApplet.CENTER, PApplet.CENTER);
		    parent.text(ontext, textpos[0], textpos[1]);
		} else {
			parent.fill(basecolor);
	    	parent.rect(x, y, size_x, size_y, roundness);
	    	parent.fill(textcolor);
	    	parent.textAlign(PApplet.CENTER, PApplet.CENTER);
		    parent.text(offtext, textpos[0], textpos[1]);
	    }
	    
	}
	
	
	boolean overRect(int x, int y, int width, int height) {
	    if (parent.mouseX >= x && parent.mouseX <= x+width && 
	    		parent.mouseY >= y && parent.mouseY <= y+height) {
	      return true;
	    } else {
	      return false;
	    }
	  }
	
	public void set(boolean st){
		state = st;
		listener.valueChanged(id, state);
	} 
	
}
