package com.jg.sound.app;

import processing.core.*;

public class Gui_Label {
	PApplet parent;
	String text; 
	int x, y;
	float textsize;


	Gui_Label(PApplet p, String txt, int ix, int iy, float itextsize){
		parent = p;
		text = txt;
		x = ix;
		y = iy;
		textsize = itextsize; 
							
		
	}
	
	void update(){
		PFont myfont = parent.createFont("SansSerif-Bold", textsize);
		parent.textFont(myfont);
	    parent.fill(255);
	    parent.textAlign(PApplet.LEFT, PApplet.CENTER);
	    parent.text(text, x, y);
	}
	
	void setText(String txt){
		text = txt;
	}
}



