package com.jg.sound.app;

import processing.core.*;

interface Gui_Slider_Listener{
	public void valueChanged(String id, float newval);
}


public class Gui_Slider{

  PApplet parent;
  Gui_Slider_Listener listener;
	
  String id;
  int swidth, sheight;    // width and height of bar
  float xpos, ypos;       // x and y position of bar
  int bottom, right;
  float spos, newspos;    // x position of slider
  float sposMin, sposMax; // max and min values of slider
  float knobwidth;
  int loose;              // how loose/heavy
  boolean over;           // is the mouse over the slider?
  boolean locked;
  float ratio;
  float last_value;

  Gui_Slider (PApplet p, Gui_Slider_Listener l, String ident, float xp, float yp, int sw, int sh, int lo) {
	id = ident;
	listener = l;
	parent = p;	
    swidth = sw;
    sheight = sh;
    knobwidth = 16;
    int widthtoheight = sw - sh;
    ratio = (float)sw / (float)widthtoheight;
    xpos = xp;
    ypos = yp;
    sposMin = xpos;
    sposMax = xpos + swidth - knobwidth;
    loose = lo;
    setValue((float)0.7); //sets spos
    newspos = spos;
    
    bottom = (int) (ypos + sheight); 
    right = (int) xpos + swidth;
    
  }

  public void update() {
    if (overEvent()) {
      over = true;
    } else {
      over = false;
    }
    if (parent.mousePressed && over) {
      locked = true;
    }
    if (!parent.mousePressed) {
      locked = false;
    }
    if (locked) {
      newspos = constrain(parent.mouseX-sheight/2, sposMin, sposMax);
    }
    if (processing.core.PApplet.abs(newspos - spos) > 1) {
      spos = spos + (newspos-spos)/loose;
    }
    
    if ( getValue() != last_value){
    	listener.valueChanged(id, getValue());
    	last_value = getValue();
    } else {
    	
    }
     
    display(); //draw it
  }

  float constrain(float val, float minv, float maxv) {
    return processing.core.PApplet.min(processing.core.PApplet.max(val, minv), maxv);
  }

  boolean overEvent() {
    if (parent.mouseX > xpos && parent.mouseX < xpos+swidth &&
       parent.mouseY > ypos && parent.mouseY < ypos+sheight) {
      return true;
    } else {
      return false;
    }
  }

  void display() {
    parent.noStroke();
//    parent.stroke(0*255,(int)(0.9 * 255),(int) (0.4 * 255));
    parent.strokeWeight(2);
    parent.stroke(0*255,(int)(0.45 * 255),(int) (0.18 * 255));
    parent.noFill();
    parent.rect(xpos, ypos, swidth, sheight, 1);
    parent.noStroke();
    parent.fill(0*255,(int)(0.76 * 255),(int) (0.31 * 255));
    parent.rect(spos, ypos, knobwidth, sheight, 1);
//    parent.fill(0, 191, 76);
//    parent.fill(28,200,20);
//    parent.rect(spos + 4, ypos + 4, knobwidth - 8, sheight - 8, 0);
  }

  float getPos() {
    // Convert spos to be values between
    // 0 and the total width of the scrollbar
    return spos * ratio;
  }
  
  public float getValue(){
	  return (spos - sposMin) / (sposMax - sposMin);
  }
  
  public void setValue(float val) {
	  spos = val * (sposMax - sposMin) + sposMin;
	  newspos = spos;
	  listener.valueChanged(id, getValue());
  }
 
}
