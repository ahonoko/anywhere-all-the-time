package com.jg.sound.app;

import processing.core.PApplet;
import android.util.Log;

public class Gui_AnimatedCircle {
	PApplet parent;
		
	float xpos, ypos;       // x and y position of bar
	float size, circle_distance;
	int circle_count;
	Circle circle[];
	int anim_speed; 		// if this is one 1 the animation advances once every frame  
	boolean animating;
	int animation_frames; // how many frames the animation has
	int col;
	
	
	Gui_AnimatedCircle(PApplet p, int x, int y, int s){
		parent = p;
		xpos = x;
		ypos = y;
		size = s;
		circle_distance = 7;
		circle_count = (int) (size / circle_distance);
		anim_speed = 50;
		
		col = parent.color(140);
		
		circle = new Circle[circle_count];
		
		for (int i = 0; i < circle_count; i++){
			float cSize = size - (circle_distance * i); 
			circle[i] = new Circle(parent, cSize);
		}
		
	}
	
	void setup(){
		animating = false;
	}
	
	
	void update(){
		draw();
	}
	
	void draw(){
		
		if (animating){
		
			int fc = parent.frameCount;
			int mod = fc % anim_speed;
			
			if (mod == 0){ // advance i.e. make next circle thick
				// find thick circle number:
				int thickCircle = 0;
				for (int i = 0; i < circle_count; i++){
					if (circle[i].thick == true){
						thickCircle = i;
						circle[i].thick = false;
					}
				}
				// set the next circle thick 
				for (int i = 0; i < circle_count; i++){
					if (thickCircle == circle_count - 1) {
						circle[0].thick = true;	
					} else {
						circle[thickCircle + 1].thick = true;	
					}
				}
			}
		} else {
			for (int i = 0; i < circle_count; i++){
				circle[i].thick = false;
			}	
		}
		
		
		for (int i = 0; i < circle_count; i++){
			circle[i].draw();
		}
		
	}
	
	
	void animate(boolean onoff){ // turn animation on or off
		animating = onoff;
	}

	
	
	
	/////////////////////////////
	// class for a single Circle
	private class Circle{
		PApplet parent;
		float size;
		float min_thickness, max_thickness;
		boolean thick;
		
	
		Circle(PApplet p, float s){
			parent = p;
			min_thickness = (float)1.5;
			max_thickness = 3;
			size = s;
			thick = false;
		}
		
		void draw(){ // draw thick or draw thin
			if(thick){
				parent.noFill();
				parent.stroke(col);
				parent.strokeWeight(max_thickness);
				parent.ellipse(xpos, ypos, size, size);
			}else{
				parent.noFill();
				parent.stroke(parent.color(col));
				parent.strokeWeight(min_thickness);
				parent.ellipse(xpos, ypos, size, size);
			}
			parent.strokeWeight(1);
		}
	}




	public void setColor(int color) {
		col = color;
	} 

	
}

